<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Looping</h1>
    <?php
        echo "<h3>Soal No 1 Looping I Love PHP </h3>";
        echo "<h5>Looping Pertama</h5>";
        
        for ($i=2;$i<=20;$i+=2){
            echo $i.". I Love PHP <br>";
        }

        echo "<h3>Soal Nomor 2 </h3>";
        for ($i=1;$i<=7;$i++){
            for ($j=1;$j<=$i;$j++){
                echo "*";
            }
            echo "<br>";
        }
    ?>
</body>
</html>
